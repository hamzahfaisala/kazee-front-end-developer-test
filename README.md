# Kazee Front-End Developer Test


# Instalation

- clone this repository to your local
- open this repository directory file with terminal
- run this command

```
npm install
npm run dev
```


# Important Routes

- "/" for Login Page
- "/dashboard" for Dasboard Page
- "/chart" for Chart Page
